package com.example.primer02;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
private TextView lblSaludo;
private EditText txtSaludo2;
private Button btnPulsame;
private Button btnlimpiame;
private Button btnCerrar;
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblSaludo= (TextView) findViewById(R.id.lblSaludo);
        txtSaludo2= (EditText) findViewById(R.id.txtSaludo2);
        btnPulsame= (Button) findViewById(R.id.btnSaludo);
        btnlimpiame= (Button)findViewById(R.id.btnlimpiame);
        btnCerrar= (Button) findViewById(R.id.btnCerrar);

        btnPulsame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtSaludo2.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Falto capturar nombre", Toast.LENGTH_SHORT).show();
                } else {
                    lblSaludo.setText("Hola " + txtSaludo2.getText().toString() + " Como estas :)");
                }
            }
        });

    btnlimpiame.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (txtSaludo2.getText().toString().matches("")){
                Toast.makeText(MainActivity.this,"Falto capturar nombre", Toast.LENGTH_SHORT).show();
            } else {

                lblSaludo.setText("");
                txtSaludo2.setText("");


                lblSaludo.setText("Hola " + txtSaludo2.getText().toString() + " Como estas :)");
            }
        }
    });

    btnCerrar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

           finish();
            }

    });
    }
}
